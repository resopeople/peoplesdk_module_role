<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\role\permission\specification\model\DefaultPermSpec;
use people_sdk\role\role\permission\model\RolePermEntityFactory;
use people_sdk\role\role\model\RoleEntityCollection;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\role\role\model\repository\RoleEntityMultiRepository;
use people_sdk\role\role\model\repository\RoleEntityMultiCollectionRepository;



return array(
    // Role permission entity services
    // ******************************************************************************

    'people_role_permission_entity_factory' => [
        'source' => RolePermEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_role_permission_entity_builder' => [
        'source' => PermSpecBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => DefaultPermSpec::class],
            ['type' => 'dependency', 'value' => 'people_role_permission_entity_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Role entity services
    // ******************************************************************************

    'people_role_entity_collection' => [
        'source' => RoleEntityCollection::class
    ],

    'people_role_entity_factory_collection' => [
        'source' => RoleEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_role_entity_factory' => [
        'source' => RoleEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_role_entity_factory_collection'],
            ['type' => 'dependency', 'value' => 'people_role_permission_entity_factory'],
            ['type' => 'config', 'value' => 'people/role/factory/role_permission_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_role_entity_multi_repository' => [
        'source' => RoleEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_role_entity_multi_collection_repository' => [
        'source' => RoleEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_role_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_role_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    RoleEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_role_entity_collection']
    ]
);