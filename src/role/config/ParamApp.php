<?php

use people_sdk\role\role\model\RoleEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'role' => [
            // Role entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see RoleEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ],

                /**
                 * Role permission entity factory execution configuration array format:
                 * @see RoleEntityFactory::setTabRolePermEntityFactoryExecConfig() configuration format.
                 */
                'role_permission_factory_execution_config' => []
            ]
        ]
    ]
);