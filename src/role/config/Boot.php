<?php

use people_sdk\module_role\role\boot\RoleBootstrap;



return array(
    'people_role_bootstrap' => [
        'call' => [
            'class_path_pattern' => RoleBootstrap::class . ':boot'
        ]
    ]
);