<?php

use people_sdk\role\requisition\request\info\factory\model\RoleConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_role_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'role' => [
            'requisition' => [
                'request' => [
                    // role requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see RoleConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'role_support_type' => 'header',
                            'role_current_include_config_key' => 'people_role_current_include',
                            'role_perm_full_update_config_key' => 'people_role_perm_full_update'
                        ]
                    ]
                ]
            ]
        ]
    ]
);