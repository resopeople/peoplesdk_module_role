<?php

use people_sdk\role\requisition\request\info\factory\model\RoleConfigSndInfoFactory;



return array(
    // Role requisition request sending information services
    // ******************************************************************************

    'people_role_requisition_request_snd_info_factory' => [
        'source' => RoleConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/role/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);