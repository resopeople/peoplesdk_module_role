<?php

use people_sdk\module_role\profile\helper\RoleProfileHelper;



return array(
    // Role profile helper services
    // ******************************************************************************

    'people_role_profile_helper' => [
        'source' => RoleProfileHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_subject_permission_entity_builder'],
            ['type' => 'dependency', 'value' => 'people_role_permission_entity_builder']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);