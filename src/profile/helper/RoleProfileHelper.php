<?php
/**
 * Description :
 * This class allows to define role profile helper class.
 * Role profile helper allows to provide features,
 * for role profile.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_role\profile\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\role\permission\subject\build\library\ToolBoxPermissionBuilder;
use liberty_code\role\role\subject\build\permission\library\ToolBoxPermissionBuilder as ToolBoxRolePermBuilder;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
use people_sdk\role\profile\model\RoleProfileEntity;



class RoleProfileHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Subject permission specification builder instance.
     * @var PermSpecBuilder
     */
    protected $objSubjPermSpecBuilder;



    /**
     * DI: Role permission specification builder instance, for role.
     * @var PermSpecBuilder
     */
    protected $objRolePermSpecBuilder;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermSpecBuilder $objSubjPermSpecBuilder
     * @param PermSpecBuilder $objRolePermSpecBuilder
     */
    public function __construct(
        PermSpecBuilder $objSubjPermSpecBuilder,
        PermSpecBuilder $objRolePermSpecBuilder
    )
    {
        // Init properties
        $this->objSubjPermSpecBuilder = $objSubjPermSpecBuilder;
        $this->objRolePermSpecBuilder = $objRolePermSpecBuilder;

        // Call parent constructor
        parent::__construct();
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate subject permission entity collections,
     * from specified role profile entity|array of role profile entities,
     * following permission specification.
     *
     * @param RoleProfileEntity|RoleProfileEntity[] $roleProfileEntity
     * @param boolean $boolClear = true
     */
    public function hydrateSubjPermEntityCollection(
        $roleProfileEntity,
        $boolClear = true
    )
    {
        ToolBoxPermissionBuilder::hydratePermissionCollection(
            $roleProfileEntity,
            $this->objSubjPermSpecBuilder,
            $boolClear
        );
    }



    /**
     * Hydrate role permission entity collections,
     * from specified roles of role profile entity|array of role profile entities,
     * following permission specification.
     *
     * @param RoleProfileEntity|RoleProfileEntity[] $roleProfileEntity
     * @param boolean $boolClear = true
     */
    public function hydrateRolePermEntityCollection(
        $roleProfileEntity,
        $boolClear = true
    )
    {
        ToolBoxRolePermBuilder::hydratePermissionCollection(
            $roleProfileEntity,
            $this->objRolePermSpecBuilder,
            $boolClear
        );
    }



}