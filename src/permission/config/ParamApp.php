<?php

use people_sdk\role\permission\specification\model\DefaultPermSpec;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'role' => [
            'permission' => [
                // Permission specification
                'specification' => [
                    /**
                     * Configuration array format:
                     * @see DefaultPermSpec configuration format.
                     */
                    'config' => []
                ]
            ]
        ]
    ]
);