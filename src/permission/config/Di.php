<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\role\permission\model\repository\PermissionRepository;
use people_sdk\role\permission\specification\model\DefaultPermSpec;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;



return array(

    // Permission services
    // ******************************************************************************

    'people_permission_repository' => [
        'source' => PermissionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_permission_specification' => [
        'source' => DefaultPermSpec::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_permission_repository'],
            ['type' => 'config', 'value' => 'people/role/permission/specification/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Subject permission entity services
    // ******************************************************************************

    'people_subject_permission_entity_factory' => [
        'source' => SubjPermEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_subject_permission_entity_builder' => [
        'source' => PermSpecBuilder::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_permission_specification'],
            ['type' => 'dependency', 'value' => 'people_subject_permission_entity_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);