<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_role\permission\library;



class ConstPermission
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'people_role_permission';



}