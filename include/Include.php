<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/permission/library/ConstPermission.php');

include($strRootPath . '/src/role/library/ConstRole.php');
include($strRootPath . '/src/role/boot/RoleBootstrap.php');

include($strRootPath . '/src/profile/library/ConstProfile.php');
include($strRootPath . '/src/profile/helper/RoleProfileHelper.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');